// Copyright 2014-2015 Isis Innovation Limited and the authors of gSLICr

#pragma once
#include "../gSLICr_defines.h"
#include "../objects/gSLICr_settings.h"
#include "../objects/gSLICr_spixel_info.h"

namespace gSLICr
{
namespace engines
{
class seg_engine
{
  public:
			seg_engine(const objects::settings& in_settings );
    virtual		~seg_engine();

    const IntImage*	Get_Seg_Mask() const
			{
			    _idx_img->UpdateHostFromDevice();
			    return _idx_img;
			};

    void		Perform_Segmentation(UChar4Image* in_img);
    virtual void	Draw_Segmentation_Result(UChar4Image* out_img){};

  protected:
    virtual void	Cvt_Img_Space(UChar4Image* inimg, Float4Image* outimg,
				      COLOR_SPACE color_space) = 0;
    virtual void	Init_Cluster_Centers() = 0;
    virtual void	Find_Center_Association() = 0;
    virtual void	Update_Cluster_Center() = 0;
    virtual void	Enforce_Connectivity() = 0;

  protected:
  // normalizing distances
    float		_max_color_dist;
    float		_max_xy_dist;

  // images
    UChar4Image*	_source_img;
    Float4Image*	_cvt_img;
    IntImage*		_idx_img;

  // superpixel map
    SpixelMap*		_spixel_map;
    int			_spixel_size;

    objects::settings	_gSLICr_settings;
};
}
}

