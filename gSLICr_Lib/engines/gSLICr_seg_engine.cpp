// Copyright 2014-2015 Isis Innovation Limited and the authors of gSLICr

#pragma once
#include "gSLICr_seg_engine.h"

using namespace std;
using namespace gSLICr;
using namespace gSLICr::objects;
using namespace gSLICr::engines;

namespace gSLICr
{
namespace engines
{
seg_engine::seg_engine(const objects::settings& in_settings)
{
    _gSLICr_settings = in_settings;
}


seg_engine::~seg_engine()
{
    if (_source_img != NULL)
	delete _source_img;
    if (_cvt_img != NULL)
	delete _cvt_img;
    if (_idx_img != NULL)
	delete _idx_img;
    if (_spixel_map != NULL)
	delete _spixel_map;
}

void
seg_engine::Perform_Segmentation(UChar4Image* in_img)
{
    _source_img->SetFrom(in_img, ORUtils::MemoryBlock<Vector4u>::CPU_TO_CUDA);
    Cvt_Img_Space(_source_img, _cvt_img, _gSLICr_settings.color_space);

    Init_Cluster_Centers();
    Find_Center_Association();

    for (int i = 0; i < _gSLICr_settings.no_iters; i++)
    {
	Update_Cluster_Center();
	Find_Center_Association();
    }

    if (_gSLICr_settings.do_enforce_connectivity)
	Enforce_Connectivity();
    cudaThreadSynchronize();
}

}
}




