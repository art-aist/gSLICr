// Copyright 2014-2015 Isis Innovation Limited and the authors of gSLICr

#include "gSLICr_seg_engine_GPU.h"
#include "gSLICr_seg_engine_shared.h"

using namespace std;
using namespace gSLICr;
using namespace gSLICr::objects;
using namespace gSLICr::engines;

// ----------------------------------------------------
//
//	kernel function defines
//
// ----------------------------------------------------

__global__ void
Cvt_Img_Space_device(const Vector4u* inimg, Vector4f* outimg,
		     Vector2i img_size, COLOR_SPACE color_space);

__global__ void
Enforce_Connectivity_device(const int* in_idx_img, int* out_idx_img,
			    Vector2i img_size);

__global__ void
Init_Cluster_Centers_device(const Vector4f* inimg, spixel_info* out_spixel,
			    Vector2i map_size, Vector2i img_size,
			    int spixel_size);

__global__ void
Find_Center_Association_device(const Vector4f* inimg,
			       const spixel_info* in_spixel_map,
			       int* out_idx_img, Vector2i map_size,
			       Vector2i img_size, int spixel_size,
			       float weight, float max_xy_dist,
			       float max_color_dist);

__global__ void
Update_Cluster_Center_device(const Vector4f* inimg, const int* in_idx_img,
			     spixel_info* accum_map,
			     Vector2i img_size, int spixel_size,
			     int no_blocks_per_line);

__global__ void
Finalize_Reduction_Result_device(const spixel_info* accum_map,
				 spixel_info* spixel_list,
				 int no_blocks_in_window);

__global__ void
Draw_Segmentation_Result_device(const int* idx_img, Vector4u* sourceimg,
				Vector4u* outimg, Vector2i img_size);

// ----------------------------------------------------
//
//	host function implementations
//
// ----------------------------------------------------

namespace gSLICr
{
namespace engines
{
seg_engine_GPU::seg_engine_GPU(const settings& in_settings)
    :seg_engine(in_settings)
{
    _source_img  = new UChar4Image(in_settings.img_size, true, true);
    _cvt_img	 = new Float4Image(in_settings.img_size, true, true);
    _idx_img	 = new IntImage(   in_settings.img_size, true, true);
    _tmp_idx_img = new IntImage(   in_settings.img_size, true, true);

    if (in_settings.seg_method == GIVEN_NUM)
    {
	float cluster_size = float(in_settings.img_size.x *
				   in_settings.img_size.y)
			   / float(in_settings.no_segs);
	_spixel_size = int(ceil(sqrtf(cluster_size)));
    }
    else
    {
	_spixel_size = in_settings.spixel_size;
    }
	
    int	     spixel_per_col = int(ceil(in_settings.img_size.x / _spixel_size));
    int	     spixel_per_row = int(ceil(in_settings.img_size.y / _spixel_size));
	
    Vector2i map_size(spixel_per_col, spixel_per_row);
    _spixel_map		  = new SpixelMap(map_size, true, true);

    _no_blocks_in_window = int(ceil(float(_spixel_size * _spixel_size * 9) /
				    float(BLOCK_DIM * BLOCK_DIM)));

    map_size.x *= _no_blocks_in_window;
    _accum_map  = new ORUtils::Image<spixel_info>(map_size, true, true);

  // normalizing factors
    _max_xy_dist = 1.0f / (1.4242f * _spixel_size); // sqrt(2) * _spixel_size
    switch (in_settings.color_space)
    {
      case RGB:
	_max_color_dist = 5.0f / (1.7321f * 255);
	break;
      case XYZ:
	_max_color_dist = 5.0f / 1.7321f; 
	break; 
      case CIELAB:
	_max_color_dist = 15.0f / (1.7321f * 128);
	break;
    }

    _max_color_dist *= _max_color_dist;
    _max_xy_dist    *= _max_xy_dist;
}

seg_engine_GPU::~seg_engine_GPU()
{
    delete _accum_map;
}


void
seg_engine_GPU::Cvt_Img_Space(UChar4Image* inimg, Float4Image* outimg,
			      COLOR_SPACE color_space)
{
    Vector4u* inimg_ptr  = inimg->GetData(MEMORYDEVICE_CUDA);
    Vector4f* outimg_ptr = outimg->GetData(MEMORYDEVICE_CUDA);
    Vector2i  img_size   = inimg->noDims;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(int(ceil(float(img_size.x) / float(blockSize.x))),
		  int(ceil(float(img_size.y) / float(blockSize.y))));

    Cvt_Img_Space_device << <gridSize, blockSize >> >(inimg_ptr, outimg_ptr,
						      img_size, color_space);

}

void
seg_engine_GPU::Init_Cluster_Centers()
{
    spixel_info* spixel_list = _spixel_map->GetData(MEMORYDEVICE_CUDA);
    Vector4f*	 img_ptr     = _cvt_img->GetData(MEMORYDEVICE_CUDA);
    Vector2i	 map_size    = _spixel_map->noDims;
    Vector2i	 img_size    = _cvt_img->noDims;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(int(ceil((float)map_size.x / float(blockSize.x))),
		  int(ceil((float)map_size.y / float(blockSize.y))));

    Init_Cluster_Centers_device << <gridSize, blockSize >> >(img_ptr,
							     spixel_list,
							     map_size,
							     img_size,
							     _spixel_size);
}

void
seg_engine_GPU::Find_Center_Association()
{
    spixel_info* spixel_list = _spixel_map->GetData(MEMORYDEVICE_CUDA);
    Vector4f*	 img_ptr     = _cvt_img->GetData(MEMORYDEVICE_CUDA);
    int*	 idx_ptr     = _idx_img->GetData(MEMORYDEVICE_CUDA);
    Vector2i	 map_size    = _spixel_map->noDims;
    Vector2i	 img_size    = _cvt_img->noDims;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(int(ceil(float(img_size.x) / float(blockSize.x))),
		  int(ceil(float(img_size.y) / float(blockSize.y))));

    Find_Center_Association_device << <gridSize, blockSize >> >(
	img_ptr, spixel_list, idx_ptr, map_size, img_size, _spixel_size,
	_gSLICr_settings.coh_weight, _max_xy_dist, _max_color_dist);
}

void
seg_engine_GPU::Update_Cluster_Center()
{
    spixel_info* accum_map_ptr	 = _accum_map->GetData(MEMORYDEVICE_CUDA);
    spixel_info* spixel_list_ptr = _spixel_map->GetData(MEMORYDEVICE_CUDA);
    Vector4f*	 img_ptr	 = _cvt_img->GetData(MEMORYDEVICE_CUDA);
    int*	 idx_ptr	 = _idx_img->GetData(MEMORYDEVICE_CUDA);
    Vector2i	 map_size	 = _spixel_map->noDims;
    Vector2i	 img_size	 = _cvt_img->noDims;

    int		 no_blocks_per_line = _spixel_size * 3 / BLOCK_DIM;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(map_size.x, map_size.y, _no_blocks_in_window);

    Update_Cluster_Center_device<<<gridSize,blockSize>>>(
	img_ptr, idx_ptr, accum_map_ptr,
	img_size, _spixel_size, no_blocks_per_line);

    dim3 gridSize2(map_size.x, map_size.y);

    Finalize_Reduction_Result_device<<<gridSize2,blockSize>>>(
	accum_map_ptr, spixel_list_ptr, _no_blocks_in_window);
}

void
seg_engine_GPU::Enforce_Connectivity()
{
    int*     idx_ptr	 = _idx_img->GetData(MEMORYDEVICE_CUDA);
    int*     tmp_idx_ptr = _tmp_idx_img->GetData(MEMORYDEVICE_CUDA);
    Vector2i img_size	 = _idx_img->noDims;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(int(ceil(float(img_size.x) / float(blockSize.x))),
		  int(ceil(float(img_size.y) / float(blockSize.y))));

    Enforce_Connectivity_device<<<gridSize, blockSize>>>(idx_ptr, tmp_idx_ptr,
							 img_size);
    Enforce_Connectivity_device<<<gridSize, blockSize>>>(tmp_idx_ptr, idx_ptr,
							 img_size);
}

void
seg_engine_GPU::Draw_Segmentation_Result(UChar4Image* out_img)
{
    Vector4u* inimg_ptr	  = _source_img->GetData(MEMORYDEVICE_CUDA);
    Vector4u* outimg_ptr  = out_img->GetData(MEMORYDEVICE_CUDA);
    int*      idx_img_ptr = _idx_img->GetData(MEMORYDEVICE_CUDA);
	
    Vector2i img_size = _idx_img->noDims;

    dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
    dim3 gridSize(int(ceil(float(img_size.x) / float(blockSize.x))),
		  int(ceil(float(img_size.y) / float(blockSize.y))));

    Draw_Segmentation_Result_device<<<gridSize,blockSize>>>(idx_img_ptr,
							    inimg_ptr,
							    outimg_ptr,
							    img_size);
    out_img->UpdateHostFromDevice();
}

}	// nmespace engines
}	// namespace gSLICr

// ----------------------------------------------------
//
//	device function implementations
//
// ----------------------------------------------------

__global__ void
Cvt_Img_Space_device(const Vector4u* inimg, Vector4f* outimg,
		     Vector2i img_size, COLOR_SPACE color_space)
{
    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;
    
    if (x < img_size.x && y < img_size.y)
	cvt_img_space_shared(inimg, outimg, img_size, x, y, color_space);
}

__global__ void
Draw_Segmentation_Result_device(const int* idx_img, Vector4u* sourceimg,
				Vector4u* outimg, Vector2i img_size)
{
    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;

    if (0 < x && x < img_size.x - 1 && 0 < y && y < img_size.y - 1)
	draw_superpixel_boundry_shared(idx_img, sourceimg, outimg,
				       img_size, x, y);
}

__global__ void
Init_Cluster_Centers_device(const Vector4f* inimg, spixel_info* out_spixel,
			    Vector2i map_size, Vector2i img_size,
			    int spixel_size)
{
    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < map_size.x && y < map_size.y)
	init_cluster_centers_shared(inimg, out_spixel, map_size, img_size,
				    spixel_size, x, y);
}

__global__ void
Find_Center_Association_device(const Vector4f* inimg,
			       const spixel_info* in_spixel_map,
			       int* out_idx_img, Vector2i map_size,
			       Vector2i img_size, int spixel_size,
			       float weight, float max_xy_dist,
			       float max_color_dist)
{
    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < img_size.x && y < img_size.y)
	find_center_association_shared(inimg, in_spixel_map, out_idx_img,
				       map_size, img_size, spixel_size, weight,
				       x, y, max_xy_dist, max_color_dist);
}

__global__ void
Update_Cluster_Center_device(const Vector4f* inimg, const int* in_idx_img,
			     spixel_info* accum_map,
			     Vector2i img_size, int spixel_size,
			     int no_blocks_per_line)
{
  // Invoked with the following block and grid configurations:
  //
  //   (0) blocks in search window consisting of 9 spixels
  //
  //         no_blocks_per_line   = (3*_spixel_size) / BLOCKDIM
  //         _no_blocks_in_window = no_blocks_per_line^2
  //         
  //   (1) dim3 blockSize(BLOCK_DIM, BLOCK_DIM);
  //   
  //         blockDim.x = BLOCK_DIM
  //         blockDIm.y = BLOCK_DIM
  //         
  //         0 <= threadIdx.x < BLOCK_DIM
  //         0 <= threadIdx.y < BLOCK_DIM
  //         threadIdx.z = 0
  //         
  //   (2) dim3 gridSize(map_size.x, map_size.y, _no_blocks_in_window);
  //
  //	     gridDim.x = map_size.x
  //	     gridDim.y = map_size.y
  //	     gridDim.z = _no_blocks_in_window
  //
  //         0 <= blockIdx.x < map_size.x
  //         0 <= blockIdx.y < map_size.y
  //         0 <= blockIdx.z < _no_blocks_in_window

    __shared__ Vector4f	color_shared[BLOCK_DIM * BLOCK_DIM];
    __shared__ Vector2f	xy_shared[   BLOCK_DIM * BLOCK_DIM];
    __shared__ int	count_shared[BLOCK_DIM * BLOCK_DIM];
    __shared__ bool	should_add; 

  // ID of pixel within the block
    const int local_id = threadIdx.y * blockDim.x + threadIdx.x;

    color_shared[local_id] = Vector4f(0, 0, 0, 0);
    xy_shared[   local_id] = Vector2f(0, 0);
    count_shared[local_id] = 0;
    should_add		   = false;
    __syncthreads();

  // ID of spixel within the grid
    const int	spixel_id = blockIdx.y * gridDim.x + blockIdx.x;
    
  // Relative block position in the search window
    const int	block_x = blockIdx.z % no_blocks_per_line;
    const int	block_y = blockIdx.z / no_blocks_per_line;

  // Pixel offset w.r.t. upper-left corner of search window
    const int	x_offset = block_x * blockDim.x + threadIdx.x;
    const int	y_offset = block_y * blockDim.y + threadIdx.y;

    if (x_offset < spixel_size * 3 && y_offset < spixel_size * 3)
    {
      // compute the start of the search window
	const int x_start = blockIdx.x * spixel_size - spixel_size;	
	const int y_start = blockIdx.y * spixel_size - spixel_size;

	const int x_img = x_start + x_offset;
	const int y_img = y_start + y_offset;

	if (x_img >= 0 && x_img < img_size.x &&
	    y_img >= 0 && y_img < img_size.y)
	{
	    const int img_idx = y_img * img_size.x + x_img;

	    if (in_idx_img[img_idx] == spixel_id)
	    {
		color_shared[local_id] = inimg[img_idx];
		xy_shared[   local_id] = Vector2f(x_img, y_img);
		count_shared[local_id] = 1;
		should_add	       = true;
	    }
	}
    }
    __syncthreads();

    if (should_add)
    {
	if (local_id < 128)
	{
	    color_shared[local_id] += color_shared[local_id + 128];
	    xy_shared[local_id]	   += xy_shared[   local_id + 128];
	    count_shared[local_id] += count_shared[local_id + 128];
	}
	__syncthreads();

	if (local_id < 64)
	{
	    color_shared[local_id] += color_shared[local_id + 64];
	    xy_shared[local_id]	   += xy_shared[   local_id + 64];
	    count_shared[local_id] += count_shared[local_id + 64];
	}
	__syncthreads();

	if (local_id < 32)
	{
	    color_shared[local_id] += color_shared[local_id + 32];
	    color_shared[local_id] += color_shared[local_id + 16];
	    color_shared[local_id] += color_shared[local_id + 8];
	    color_shared[local_id] += color_shared[local_id + 4];
	    color_shared[local_id] += color_shared[local_id + 2];
	    color_shared[local_id] += color_shared[local_id + 1];

	    xy_shared[local_id] += xy_shared[local_id + 32];
	    xy_shared[local_id] += xy_shared[local_id + 16];
	    xy_shared[local_id] += xy_shared[local_id + 8];
	    xy_shared[local_id] += xy_shared[local_id + 4];
	    xy_shared[local_id] += xy_shared[local_id + 2];
	    xy_shared[local_id] += xy_shared[local_id + 1];

	    count_shared[local_id] += count_shared[local_id + 32];
	    count_shared[local_id] += count_shared[local_id + 16];
	    count_shared[local_id] += count_shared[local_id + 8];
	    count_shared[local_id] += count_shared[local_id + 4];
	    count_shared[local_id] += count_shared[local_id + 2];
	    count_shared[local_id] += count_shared[local_id + 1];
	}
    }
    __syncthreads();

    if (local_id == 0)
    {
	const int accum_map_idx = spixel_id * gridDim.z + blockIdx.z;

	accum_map[accum_map_idx].center     = xy_shared[0];
	accum_map[accum_map_idx].color_info = color_shared[0];
	accum_map[accum_map_idx].no_pixels  = count_shared[0];
    }
}

__global__ void
Finalize_Reduction_Result_device(const spixel_info* accum_map,
				 spixel_info* spixel_list,
				 int no_blocks_in_window)
{
    Vector2i	map_size(gridDim.x, gridDim.y);

    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < map_size.x && y < map_size.y)
	finalize_reduction_result_shared(accum_map, spixel_list, map_size,
					 no_blocks_in_window, x, y);
}

__global__ void
Enforce_Connectivity_device(const int* in_idx_img, int* out_idx_img,
			    Vector2i img_size)
{
    const int	x = threadIdx.x + blockIdx.x * blockDim.x;
    const int	y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < img_size.x && y < img_size.y)
	supress_local_lable(in_idx_img, out_idx_img, img_size, x, y);
}

